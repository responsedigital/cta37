<?php

namespace Fir\Pinecones\Cta37;

use Fir\Utils\GlobalFields;

class Schema
{
    public static function getACFLayout()
    {
        return [
            'name' => 'Cta37',
            'label' => 'Pinecone: CTA 37',
            'sub_fields' => [
                [
                    'label' => 'General',
                    'name' => 'generalTab',
                    'type' => 'tab',
                    'placement' => 'left',
                    'endpoint' => 0,
                ],
                [
                    'label' => 'Overview',
                    'name' => 'overview',
                    'type' => 'message',
                    'message' => "A contact section that contains both a map and a form."
                ],
                [
                    'label' => 'Content',
                    'name' => 'contentTab',
                    'type' => 'tab',
                    'placement' => 'left',
                    'endpoint' => 0,
                ],
                [
                    'label' => 'Title',
                    'name' => 'title',
                    'type' => 'text'
                ],
                [
                    'label' => 'Text',
                    'name' => 'text',
                    'type' => 'textarea'
                ],
                [
                    'label' => 'Map',
                    'name' => 'map',
                    'type' => 'google_map'
                ],
                GlobalFields::getGroups(array(
                    'priorityGuides',
                    'ID'
                )),
                [
                    'label' => 'Options',
                    'name' => 'optionsTab',
                    'type' => 'tab',
                    'placement' => 'top',
                    'endpoint' => 0
                ],
                [
                    'label' => '',
                    'name' => 'options',
                    'type' => 'group',
                    'layout' => 'row',
                    'sub_fields' => [
                        GlobalFields::getOptions(array(
                            'hideComponent'
                        ))
                    ]
                ]
            ]
        ];
    }
}
