<!-- Start CTA 37 -->
@if(!Fir\Utils\Helpers::isProduction())
<!-- Description : A contact section that contains both a map and a form. -->
@endif
<div class="cta-37"  is="fir-cta-37">
  <script type="application/json">
      {
          "options": @json($options)
      }
  </script>
  <img class="cta-37__map" src="{{ $post['featured_image'] ?: 'https://res.cloudinary.com/response-mktg/image/upload/q_auto,f_auto/v1594057172/fir/demos/map-wide.jpg' }}" alt="Map">
  <div class="cta-37__wrap">
      <h2 class="cta-37__title">{{ $title ?: $faker->text($maxNbChars = 20) }}</h2>
      <p class="cta-37__text">{{ $text ?: $faker->paragraph($nbSentences = 2, $variableNbSentences = true) }}</p>
      <form class="cta-37__form">
        <input type="text" placeholder="Name">
        <input type="email" placeholder="Email">
        <textarea placeholder="Message"></textarea>
        <input class="cta-37__form-submit" type="submit">
      </form>
  </div>
</div>
<!-- End CTA 37 -->
