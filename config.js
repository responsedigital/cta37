module.exports = {
    'name'  : 'CTA 37',
    'camel' : 'Cta37',
    'slug'  : 'cta-37',
    'dob'   : 'CTA_37_1440',
    'desc'  : 'A contact section that contains both a map and a form.',
}